arr = [0]
for i in range(1,1001):
    arr.append(i)
def find_head(arr,i):
    if arr[i] != i:
        return find_head(arr,arr[i])
    else:
        return i

n,m = map(int,input().split())
if n == 1:
    print(0)
    exit(0)

nodes_set = set({})
for i in range(0,m):
    a,b,c = map(int,input().split())
    nodes_set.add((c,a,b))

nodes_set = sorted(nodes_set)
price = 0
count = 0
for nodes in nodes_set:
    x = nodes[1]
    y = nodes[2]

    x = find_head(arr,x)
    y = find_head(arr,y)

    if x != y:
        arr[x]=y
        count+=1
        price+=nodes[0]
        if(count==n-1):
            break

print(price)